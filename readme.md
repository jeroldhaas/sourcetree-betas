-----------------------
SourceTree Beta Program
-----------------------

This repository hosts beta versions of SourceTree. Beta versions are often developed in conjunction with Atlassian hack-a-thons and ShipIt Days, but are provided to you completely “as-is”, with all bugs and errors, and are not supported by Atlassian. Unless licensed otherwise by Atlassian, the beta versions of SourceTree are subject to Section 11 of the Atlassian Customer Agreement (https://www.atlassian.com/legal/customer-agreement/) and are “No-Charge Products” as defined therein.


After downloading and installing your preferred beta client, you can stay up-to-date through either of these two ways: 
* Use the beta version to get in app prompts to update to the latest beta version 
* Clone the SourceTree Betas repo in SourceTree to get in app notifications of any changes 

Your beta release will be installed as SourceTree Beta. Please keep in mind that beta releases will have features in early preview, and may have stability or performance issues. Please report any issues here: https://jira.atlassian.com/secure/RapidBoard.jspa?rapidView=1574.